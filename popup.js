function Popup(){
	var Popup = this;
	this.popup = document.createElement('div');
	this.popup.classList.add('popup');
	this.overlay = document.createElement('div');
	this.overlay.classList.add('overlay');
	document.body.appendChild(this.popup);
	document.body.appendChild(this.overlay);

    this.open = function(content){
    	Popup.popup.innerHTML = content;
    }
    
    this.close = function(){
    	document.body.removeChild(this.popup);
    	document.body.removeChild(this.overlay);
    }
    this.overlay.onclick = function(){
    	Popup.close();
    }
}